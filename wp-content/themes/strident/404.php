<?php
/*
 * The single post page for the theme
 *
 * @package WordPress
 * @subpackage Strident
 */
?>

<?php get_header(); ?>

<body <?php body_class(); ?>>

	<header id="header" role="header">

		<nav id="access" role="navigation">
			<div class="navigation">
				<?php wp_nav_menu(); ?>
			</div><!-- .navigation -->
			<?php get_template_part( '_contact' ); ?>
		</nav>

		<div id="branding">
			<div class="page-title">
				<h1>Error: Page not set to an instance of a page</h1>
			</div><!-- .page-title-->
			<?php get_template_part( '_social' ); ?>
		</div><!-- .branding -->

	</header>

	<section id="content">

		<section id="main" role="main">

			<article id="post-0">
				<header class="entry-header red">
					<h1 class="entry-title">404: Page not found</h1>
				</header><!-- .entry-header -->
				<section class="entry-content">
					<p>We are sorry but we could not find the page you require.</p>
				</section><!-- .entry-summary -->
				<footer class="entry-content">

				</footer><!-- entry-footer -->
			</article><!-- .post -->

		</section><!-- #main -->

		<?php get_sidebar(); ?>

	</section><!-- #content -->

<?php get_footer(); ?>