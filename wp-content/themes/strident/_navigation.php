<nav id="posts-nav" class="navigation">
  <div class="nav-previous"><?php next_posts_link( 'Older Posts' ); ?></div>
  <div class="nav-next"><?php previous_posts_link( 'Newer Posts' ); ?></div>
</nav>