<?php
/*
 * The header file for the theme
 * 
 * @package WordPress
 * @subpackage Strident
 */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<!-- Meta-Data -->
		<meta charset="<?php bloginfo('charset'); ?>">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="author" content="Adam Chamberlin">
		<meta name="description" content="SharePoint">
		<meta name="viewport" content="maximum-scale=1.0, width=device-width, initial-scale=1.0">
		
		<!-- Title -->
		<title><?php 
			if(is_single()) { single_post_title(); }
			elseif(is_home() || is_front_page()) { bloginfo('name'); print ' | '; bloginfo('description'); get_page_number(); }
			elseif(is_page()) { single_post_title(''); }
			elseif(is_search()) { bloginfo('name'); print ' | Search results for ' . wp_specialchars($s); get_page_number(); }
			elseif(is_404()) { bloginfo('name'); print ' | Not Found'; }
			else { bloginfo('name'); wp_title('|'); get_page_number(); }
		?></title>
		
		<!-- Links -->
		<link rel="stylesheet" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
		
		<?php wp_enqueue_script( 'jquery' ); ?>
		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		
		<?php if(is_singular()) wp_enqueue_script('comment_reply'); //Include JavaScript for threaded comments ?>
		
		<?php wp_head(); //Include the WordPress header hook ?>

		<script language="javascript" type="text/javascript">
			jQuery(document).ready(function($) {
				$('#showSearch').click(function () {
					$('#search').toggle('slow');
    				});  
			});

			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-33585120-1']);
			_gaq.push(['_trackPageview']);

			(function() {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();

		</script>
		
	</head>