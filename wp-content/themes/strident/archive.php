<?php
/*
 * The archive file for the theme
 * 
 * @package WordPress
 * @subpackage Strident
 */
?>

<?php get_header(); ?>

<body <?php body_class(); ?>>

<?php the_post(); ?>
		
	<header id="header" role="header">
		
		<nav id="access" role="navigation">
			<div class="navigation">
				<?php wp_nav_menu(); ?>
			</div><!-- .navigation -->
			<?php get_template_part( '_contact' ); ?>
		</nav>

		<div id="branding">
			<div class="page-title">
				<?php // If is day
				if ( is_day() ) : ?>
					<h1>Daily Archives: <span><?php echo get_the_time( get_option( 'date_format' ) ); ?></span></h1>

				<?php // If is month
				elseif ( is_month() ) : ?>
					<h1>Monthly Archives: <span><?php echo get_the_time( 'F Y' ); ?></span></h1>
				<?php // If is year
				elseif ( is_year() ) : ?>
					<h1>Yearly Archives: <span><?php echo get_the_time( 'Y' ); ?></span></h1>
				<?php // For everything else
				else : ?>
					<h1>News and Views Archives</h1>
				<?php // End the if
				endif; ?>
			</div><!-- .page-title-->
			<?php get_template_part( '_social' ); ?>
		</div><!-- .branding -->
		
	</header>

	<section id="content">

		<section id="main" class="archive-post" role="main">

			<?php rewind_posts(); ?>
		
			<?php if ( have_posts() ) : ?>
				<?php while ( have_posts() ) : the_post(); ?>
						<?php $bgcolour = ( in_category( 'News' ) ) ? "t-green" : "t-orange"; ?>
						<a href="<?php the_permalink(); ?>" id="post-<?php the_ID(); ?>" <?php post_class("tile w280 {$bgcolour}"); ?>>
							<h3 class="service-link"><?php the_title(); ?></h3>
							<time datetime="<?php the_time( 'Y-m-d' ); ?>">Posted on <?php the_time( 'j' ); ?> <?php the_time( 'F' ); ?> <?php the_time( 'Y' ); ?></time>
						</a><!-- .post-<?php the_ID(); ?> -->
					<?php 
					wp_reset_postdata(); ?>
				<?php endwhile; ?>
				<?php else : ?>
					<div id="post-0" <?php post_class(); ?>>
						<div class="entry-content">
							<p>There are no posts to be displayed.</p>
						</div><!-- .entry-content -->
					</div><!-- .post -->
			<?php endif; ?>

			<?php get_template_part('_navigation'); ?>

		</section><!-- #main -->

		<?php get_sidebar(); ?>
		
	</section><!-- #content -->

<?php get_footer(); ?>