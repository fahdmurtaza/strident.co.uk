<?php
/*
 * The index file for the theme
 * 
 * The home page for news and views
 *
 */
?>

<?php get_header(); ?>

<body <?php body_class(); ?>>

	<header id="header" role="header">
		
		<nav id="access" role="navigation">
			<div class="navigation">
				<?php wp_nav_menu(); ?>
			</div><!-- .navigation -->
			<?php get_template_part( '_contact' ); ?>
		</nav>

		<div id="branding">
			<div class="page-title">
				<h1>News and Views</h1>
			</div><!-- .page-title-->
			<?php get_template_part( '_social' ); ?>
		</div><!-- .branding -->
		
	</header>

	<section id="content">

		<section id="news" class="home-post" role="main">

			<header class="entry-header green">
				<h1 class="entry-title">
					Latest news
				</h1>
			</header><!-- .entry-header -->

			<?php // Start the loop
			$news_query = new WP_Query( 'category_name=news&posts_per_page=5');

			while ( $news_query->have_posts()) : $news_query->the_post() ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<aside class="entry-meta">
							<time datetime="<?php the_time( 'Y-m-d' ); ?>"><span class="date-day"><?php the_time( 'j' ); ?></span> <span class="date-month"><?php the_time( 'F' ); ?></time>
						</aside><!-- .entry-meta -->
						<h3 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" rel="bookmark"><?php the_title(); ?></a></h3>
					</header><!-- .entry-header -->
					<section class="entry-summary">
						<?php the_excerpt(); ?>
					</section><!-- .entry-summary -->
					<footer class="entry-footer">
						<a class="more-link" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" rel="bookmark">Continue reading</a>
					</footer><!-- entry-footer -->
				</article><!-- .post -->

			<?php 
			endwhile;
			wp_reset_postdata(); ?>

			<footer>
				<h3><a href="<?php bloginfo('url'); ?>/archives/category/news">See more news articles</a></h3>
			</footer>

		</section>

		<section id="views" class="home-post">

			<header class="entry-header orange">
				<h1 class="entry-title">
					Latest views
				</h1>
			</header><!-- .entry-header -->

			<?php // Start the loop
			$blog_query = new WP_Query( 'category_name=views&posts_per_page=5');

			while ( $blog_query->have_posts()) : $blog_query->the_post() ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<aside class="entry-meta">
							<time datetime="<?php the_time( 'Y-m-d' ); ?>"><span class="date-day"><?php the_time( 'j' ); ?></span> <span class="date-month"><?php the_time( 'F' ); ?></time>
						</aside><!-- .entry-meta -->
						<h3 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" rel="bookmark"><?php the_title(); ?></a></h3>
					</header><!-- .entry-header -->
					<section class="entry-summary">
						<?php the_excerpt(); ?>
					</section><!-- .entry-summary -->
					<footer class="entry-footer">
						<a class="more-link" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" rel="bookmark">Continue reading</a>
					</footer><!-- entry-footer -->
				</article><!-- .post -->

			<?php 
			endwhile;
			wp_reset_postdata(); ?>

			<footer>
				<h3><a href="<?php bloginfo('url'); ?>/archives/category/views">See more views articles</a></h3>
			</footer>

		</section>
		
	</section><!-- #content -->

<?php get_footer(); ?>