<div class="social">
  <a href="#" title="Search this site" class="search" id="showSearch">Search</a>
	<a href="https://support.strident.co.uk/Account/LogOn" title="Client Login" class="client">Client Login</a>
	<a href="<?php bloginfo('rss2_url'); ?>" title="RSS Feed" class="rss">RSS Feed</a>
	<a href="http://www.linkedin.com/company/strident-computer-systems" title="Linked In" class="linked-in">Linked In</a>
	<a href="https://twitter.com/stridentcs" title="Twitter" class="twitter">Twitter</a>
</div><!-- .social -->
<div id="search">
	<form role="search" method="get" id="searchform" action="<?php bloginfo('siteurl'); ?>">
		<input type="search" value="" name="s" id="s" placeholder="Search this site" />
	</form>
</div><!-- #search -->	