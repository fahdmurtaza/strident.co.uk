<?php
/*
 * The sidebar file for the theme
 * 
 * @package WordPress
 * @subpackage Strident
 */
?>

<aside id="sidebar">

  <div class="sidebar-tile t-purple contact">
    <h3>Contact Strident</h3>
    <a href="tel:01473835281" title="Call Strident">01473 835 281</a>
  </div>
  
  <?php // If is a page
  if ( is_page() ) : ?>
    <div class="sidebar-tile t-orange news">
    <h3>News and Views</h3>
    <ul class="post-list">
  <?php
  $args = array(
  'numberposts' => 3,
  'post_type' => 'post',
  );
  $posts_array = get_posts( $args );
  if ( !empty( $posts_array)  ) {
  foreach( $posts_array as $post) : setup_postdata( $post );?>
  <li><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></li>
  <?php endforeach;
  } else {
  echo "<li>No news to report</li>";
  } ?>
  </ul>
  </div><!-- .tile -->
  
  <div class="sidebar-tile t-purple">
  <h3>Business Talk</h3>
  <ul class="post-list">
  <?php
  $args = array(
  'numberposts' => 5,
  'post_type' => 'business_talk',
  );
  $posts_array = get_posts( $args );
  if ( !empty( $posts_array)  ) {
  foreach( $posts_array as $post) : setup_postdata( $post );?>
  <li><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></li>
  <?php endforeach;
  } else {
  echo "<li>No news to report</li>";
  } ?>
  </ul>
  </div><!-- .tile -->
  
  <div class="sidebar-tile t-blue">
  <h3>Contact Strident</h3>
  <?php echo do_shortcode( '[contact-form-7 id="390" title="Main contact form"]' ); ?>
  </div><!-- .tile -->
  <?php else : ?>
  
  <?php dynamic_sidebar('post-sidebar'); ?>
  
  <?php endif; ?>

</aside><!-- #sidebar -->