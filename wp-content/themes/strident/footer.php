<?php
/*
 * The footer file for the theme
 *
 * @package WordPress
 * @subpackage Strident
 */
?>

			<footer id="footer" role="contentinfo">
				<?php wp_nav_menu(array('theme_location' => 'footer')); ?>
			</footer>

	</body>

</html>