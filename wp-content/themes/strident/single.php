<?php
/*
 * The single post page for the theme
 * 
 * @package WordPress
 * @subpackage Strident
 */
?>

<?php get_header(); ?>

<body <?php body_class(); ?>>

	<?php the_post(); ?>
		
	<header id="header" role="header">
		
		<nav id="access" role="navigation">
			<div class="navigation">
				<?php wp_nav_menu(); ?>
			</div><!-- .navigation -->
			<?php get_template_part( '_contact' ); ?>
		</nav>

		<div id="branding">
			<div class="page-title">
				<h1><?php the_title(); ?></h1>
			</div><!-- .page-title-->
			<?php get_template_part( '_social' ); ?>
		</div><!-- .branding -->
		
	</header>

	<section id="content">

		<section id="main" class="single-post" role="main">

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<?php
						$colours = array( 1 => 'blue', 2 => 'green', 3 => 'orange', 5 => 'purple', 6 => 'red');
						$colour = $colours[ rand( 1, 6 ) ];
					?>
				<header class="entry-header <?php echo $colour; ?>">
					<h1 class="entry-title">
						<?php the_title(); ?>
					</h1>
				</header><!-- .entry-header -->
				<section class="entry-content">
					<?php the_content(); ?>
				</section><!-- .entry-summary -->
				<footer class="entry-footer">
					<div class="entry-utility">
						<?php printf(__('This entry was posted in %1$s%2$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>. Follow any comments here with the <a href="%5$s" title="Comments RSS to %4$s" rel="alternate" type="application/rss+xml">RSS feed for this post</a>.', 'inception'),
							get_the_category_list(', '),
							get_the_tag_list(__(' and tagged ', 'inception'), ', ', '' ),
							get_permalink(),
							the_title_attribute('echo=0'),
							comments_rss()); ?>
						<?php if(('open' == $post->comment_status) && ('open' == $post->ping_status)) : // Comments and trackbacks open ?>
	                        <?php printf(__('<a class="comment-link" href="#respond" title="Post a comment">Post a comment</a> or leave a trackback: <a class="trackback-link" href="%s" title="Trackback URL for your post" rel="trackback">Trackback URL</a>.', 'inception'), get_trackback_url()); ?>
						<?php elseif(!('open' == $post->comment_status) && ('open' == $post->ping_status)) : // Only trackbacks open ?>
	                        <?php printf(__('Comments are closed, but you can leave a trackback: <a class="trackback-link" href="%s" title="Trackback URL for your post" rel="trackback">Trackback URL</a>.', 'inception'), get_trackback_url()); ?>
						<?php elseif(('open' == $post->comment_status) && !('open' == $post->ping_status)): // Only comments open ?>
	                        <?php _e('Trackbacks are closed, but you can <a class="comment-link" href="#respond" title="Post a comment">post a comment</a>.', 'inception'); ?>
						<?php elseif(!('open' == $post->comment_status) && !('open' == $post->ping_status)) : // Comments and trackbacks closed ?>
	                        <?php _e('Both comments and trackbacks are currently closed.', 'inception') ?>
						<?php endif; ?>
						<?php edit_post_link(__('Edit', 'inception' ), "<span class=\"edit-link\">", "</span>"); ?>
					</div><!-- .entry-utility -->
				</footer><!-- entry-footer -->
			</article><!-- .post -->

			<?php //comments_template('', true); //Load the comments ?>

		</section><!-- #main -->

		<?php get_sidebar(); ?>
		
	</section><!-- #content -->

<?php get_footer(); ?>