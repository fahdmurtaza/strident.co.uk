<?php
/**
 * Template Name: Cloud Computing Solution Group Page
 *
 * This template is used for the Communications Solution selector page
 *
 */

get_header(); ?>

<body <?php body_class(); ?>>

	<?php //Get the post
		the_post(); ?>
		
	<header id="header" role="header">
		
		<nav id="access" role="navigation">
			<div class="navigation">
				<?php wp_nav_menu(); ?>
			</div><!-- .navigation -->
			<?php get_template_part( '_contact' ); ?>
		</nav>

		<div id="branding">
			<div class="page-title">
				<h1><?php the_title(); ?></h1>
			</div><!-- .page-title-->
			<?php get_template_part( '_social' ); ?>
		</div><!-- .branding -->
		
	</header>

	<section id="content" class="comm">
	  <h1 class="solution-heading">See how Cloud Computing can reduce IT risks...</h1>
		<?php
			global $post;
			$args = array(
						'numberposts' => 100,
						'post_type' => 'page',
						'post_parent' => $post->ID,
						'orderby' => 'menu_order',
						'order' => 'ASC'
					);
			$posts_array = get_posts( $args );
			foreach( $posts_array as $post) : setup_postdata( $post );?>
				<?php 
					$width = get_post_meta( $post->ID, 'tile_width', true );
					$colour = get_post_meta( $post->ID, 'tile_colour', true);
				?>
				<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="tile<?php if (!empty( $width )) { echo " " . $width; }  if (!empty( $colour )) { echo " t-" . $colour; } ?> ">
					<h3 class="service-link"><?php the_title(); ?></h3>
					<p><?php echo get_post_meta( $post->ID, 'tile_copy', true); ?></p>
				</a><!-- .tile -->
			
			<?php endforeach; ?>
		<a href="<?php site_url( '/' ); ?>/about/case-studies" title="Case Studies" class="tile w220 t-blue">
			<h3 class="service-link">Case Studies</h3>
			<p>View some of our case studies for various clients and projects</p>
		</a>
	</section>

<?php get_footer(); ?>