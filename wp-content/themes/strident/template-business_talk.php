<?php
/*
 * Template Name: Business Talk Page
 * 
 * This template is used for the Business Talk main page
 *
 */
?>

<?php get_header(); ?>

<body <?php body_class(); ?>>

	<?php //Get the post
		the_post(); ?>
		
	<header id="header" role="header">
		
		<nav id="access" role="navigation">
			<div class="navigation">
				<?php wp_nav_menu(); ?>
			</div><!-- .navigation -->
			<?php get_template_part( '_contact' ); ?>
		</nav>

		<div id="branding">
			<div class="page-title">
				<h1><?php the_title(); ?></h1>
			</div><!-- .page-title-->
			<?php get_template_part( '_social' ); ?>
		</div><!-- .branding -->
		
	</header>

<section id="content">
		
	<section id="main" class="single-page" role="main">
		
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<header class="entry-header purple">
					<h1 class="entry-title">
						<?php the_title(); ?>
					</h1>
				</header><!-- .entry-header -->
				<div class="entry-content">
					<?php the_content(); ?>
				</div><!-- .entry-content -->
			</article><!-- #post-<?php the_ID(); ?> -->

			<?php // Start the loop
			$bi_query = new WP_Query( 'post_type=business_talk');

			while ( $bi_query->have_posts()) : $bi_query->the_post() ?>

			<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" rel="bookmark" id="post-<?php the_ID(); ?>" class="tile business-talk-tile">
				<header class="entry-header">
					<h3 class="entry-title"><?php the_title(); ?></h3>
				</header><!-- .entry-header -->
				<section class="entry-summary">
					<?php if( has_post_thumbnail()) {
						the_post_thumbnail( 'business-talk-thumb' );
						the_excerpt();
					} ?>
				</section><!-- .entry-summary -->
			</a><!-- .post -->

		<?php 
		endwhile; ?>

	</section><!-- #main -->

  <?php include (TEMPLATEPATH . '/sidebar-business_talk.php'); ?>
	
</section><!-- #content -->

<?php get_footer(); ?>