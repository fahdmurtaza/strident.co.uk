<?php
/*
 * The sidebar file for solution pages
 * 
 * @package WordPress
 * @subpackage Strident
 */
?>

<aside id="sidebar">

  <div class="sidebar-tile t-purple contact">
    <h3>Contact Strident</h3>
    <a href="tel:01473835281" title="Call Strident">01473 835 281</a>
  </div>
  
  <?php // Get the tag for the current page
  $tags = wp_get_post_tags( $post->ID, array( 'fields' =>'ids' ) );
  $solution = get_post_custom_values( 'solution_tag', $post->ID );
  ?>
  
  <div class="sidebar-tile t-orange">
    <h3>Resources</h3>
    <ul class="post-list">
    <?php
    if ( !empty( $solution )) {
    $news_query = new WP_Query( 'posts_per_page=5&tag=' . $solution[0] );
    if ( $news_query->have_posts() ) {
    while ( $news_query->have_posts()) : $news_query->the_post() ?>
    <li><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></li>
    <?php 
    endwhile;
    wp_reset_postdata();
    } else {
    echo "<li>There are no related news or blog articles.</li>";
    }
    } else {
    echo "<li>This page is not tagged</li>";
    } ?>
    </ul>
  </div><!-- .related-news -->
  
  <div class="sidebar-tile t-blue">
    <h3>Related Solutions</h3>
    <ul class="post-list">
    <?php wp_list_pages( array( 'child_of' => $post->post_parent, 'exclude' => $post->ID, 'title_li' => '' ) ); ?>
    </ul>
  </div><!-- .related-solutions -->
  
  <div class="sidebar-tile t-green">
    <h3>Products and Services</h3>
    <ul class="post-list">
    <?php
    if ( !empty( $solution )) {
    $product_query = new WP_Query( 'post_type=page&tag=' . $solution[0] );
    if ( $product_query->have_posts() ) {
    while ( $product_query->have_posts()) : $product_query->the_post() ?>
    <li><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></li>
    <?php 
    endwhile;
    wp_reset_postdata();
    } else {
    echo "<li>There are no related products.</li>";
    }
    } else {
    echo "<li>This page is not tagged</li>";
    } ?>
    </ul>
  </div><!-- .related-products -->

</aside><! -- #sidebar -->