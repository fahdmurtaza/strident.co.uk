<?php
/*
 * The sidebar file for business talk pages
 * 
 * @package WordPress
 * @subpackage Strident
 */
?>

<aside id="sidebar">
	<div class="sidebar-tile t-purple">
		<h3>Get your copy</h3>
		<?php echo do_shortcode( '[contact-form-7 id="393" title="BusinessTalk contact form"]' ); ?>
	</div>
</aside>