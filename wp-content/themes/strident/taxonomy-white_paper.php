<?php
/*
 * The index file for the theme
 * 
 * The home page for news and views
 *
 */
?>

<?php get_header(); ?>

<body <?php body_class(); ?>>

	<header id="header" role="header">
		
		<nav id="access" role="navigation">
			<div class="navigation">
				<?php wp_nav_menu(); ?>
			</div><!-- .navigation -->
			<?php get_template_part( '_contact' ); ?>
		</nav>

		<div id="branding">
			<div class="page-title">
				<h1>News and Views</h1>
			</div><!-- .page-title-->
			<?php get_template_part( '_social' ); ?>
		</div><!-- .branding -->
		
	</header>

	<section id="content">

		<section id="news" class="home-post" role="main">

			<header class="entry-header green">
				<h1 class="entry-title">
					Hello World
				</h1>
			</header><!-- .entry-header -->

		</section>
		
	</section><!-- #content -->

<?php get_footer(); ?>