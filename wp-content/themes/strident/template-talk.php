<?php
/*
 * Template Name: Business Talk Landing Page
 * 
 * This template is used for the Business Talk landing page
 *
 */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<!-- Meta-Data -->
		<meta charset="<?php bloginfo('charset'); ?>">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="author" content="Adam Chamberlin">
		<meta name="description" content="SharePoint">
		<meta name="viewport" content="maximum-scale=1.0, width=device-width, initial-scale=1.0">
		
		<!-- Title -->
		<title>Strident Business Talk | <?php single_post_title(); ?></title>
		
		<!-- Links -->
		<link rel="stylesheet" media="all" href="<?php bloginfo( 'stylesheet_directory' ); ?>/css/talk.css">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
		
		<?php wp_enqueue_script( 'jquery' ); ?>
		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		
		<?php wp_head(); //Include the WordPress header hook ?>

		<script language="javascript" type="text/javascript">

			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-33585120-1']);
			_gaq.push(['_trackPageview']);

			(function() {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();

		</script>
		
	</head>

	<body>

		<?php //Get the post
			the_post(); ?>
			
		<header id="header" role="header">
			
			<nav id="access" role="navigation">
				<div class="navigation">
					<?php wp_nav_menu(); ?>
				</div><!-- .navigation -->
			</nav>

			<div class="title">
				<h1>Business Talk</h1>
				<h2><?php the_title(); ?></h2>
				<?php the_post_thumbnail(150); ?>
			</div>

		</header>

		<section id="content" role="main">

			<?php the_content(); ?>

			<h2>Business Talk Archive</h3>
			<p>You can access the entire back catalogue of Business Talk magazines from our interactive archive</p>
			<ul class="bt-archive">
				<?php // Start the loop
				$bi_query = new WP_Query( 'post_type=business_talk');
				while ( $bi_query->have_posts()) : $bi_query->the_post() ?>
					<li>
						<h3><?php the_title(); ?></h3>
						<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" rel="bookmark" id="post-<?php the_ID(); ?>">
							<?php if( has_post_thumbnail()) {
								the_post_thumbnail( 'business-talk-thumb' );
								the_excerpt();
							} ?>
						</a>
					</li>
				<?php 
				endwhile; ?>
			</ul>

			
		</section><!-- #content -->

		<footer id="footer" role="contentinfo">
			<p>Call <a href="tel:01473835280">01473 835280</a> or go online at <a href="http://www.strident.co.uk" title="Strident Computer Systems">www.strident.co.uk</a></p>
		</footer>

	</body>

</html>