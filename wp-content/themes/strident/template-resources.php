<?php
/**
 * Template Name: Resources Group Page
 *
 * This template is used for the Resources selector page
 *
 */

get_header(); ?>

<body <?php body_class(); ?>>

	<?php //Get the post
		the_post(); ?>
		
	<header id="header" role="header">
		
		<nav id="access" role="navigation">
			<div class="navigation">
				<?php wp_nav_menu(); ?>
			</div><!-- .navigation -->
			<?php get_template_part( '_contact' ); ?>
		</nav>

		<div id="branding">
			<div class="page-title">
				<h1><?php the_title(); ?></h1>
			</div><!-- .page-title-->
			<?php get_template_part( '_social' ); ?>
		</div><!-- .branding -->
		
	</header>

	<section id="content" class="resources">
	
		<a href="<?php site_url( '/' ); ?>/about/case-studies" title="Case Studies" class="tile w460 t-blue">
			<h3 class="service-link">Case Studies</h3>
			<p>View some of our case studies for various clients and projects</p>
		</a>
		
		<a href="/business-talk" title="Business Talk" class="tile t-purple w460">
      <h3 class="service-link">Business Talk</h3>
      <p>Our free quarterly magazine. Small in size but packed with information, insight and ideas to help any business raise their bar. Click here to subscribe.</p>
    </a>
    
    <a href="/resources/white-papers" title="White Papers" class="tile w300 t-red">
      <h3>White Papers</h3>
      <p>Lorem ipsum</p>
    </a>
    
    <a href="/resources/how-to" title="How To" class="tile w300 t-blue">
      <h3>How To...</h3>
      <p>Lorem ipsum</p>
    </a>
    
    <a href="/resources/videos" title="Videos" class="tile w300 t-orange">
      <h3>Videos</h3>
      <p>Lorem ipsum</p>
    </a>
    
    <a href="/resources/microsoft-guides" title="Microsoft Guides" class="tile w460 t-green">
      <h3>Microsoft Guides</h3>
      <p>Lorem ipsum</p>
    </a>
    
    <a href="/news" class="tile t-orange w460 news">
		  <h3 class="service-link">News and Views</h3>
		  <p>Keep upto date with the latest developments in Strident and the surrounding technologies</p>
		</a>
	
	</section>

<?php get_footer(); ?>